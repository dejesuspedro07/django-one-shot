from django.urls import path
from todos.views import todos_list_list, todo_list_detail, create_todo_list, edit_todo, delete_todo,todo_item_create,todo_item_update


urlpatterns = [
     path("", todos_list_list, name="todos_list_list"),
     path('<int:id>', todo_list_detail, name='todo_list_detail'),
     path("todos/create/", create_todo_list, name="create_todo_list"),
     path("<int:id>/edit/", edit_todo, name="edit_todo"),
     path("<int:id>/delete/", delete_todo, name="delete_todo"),
     path("item/create/", todo_item_create, name="todo_item_create"),
     path("items/<int:id>/edit", todo_item_update, name="todo_item_update"),
]
