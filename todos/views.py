from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm


def todos_list_list(request):
    todo_list = TodoList.objects.all
    context = {"todos_list_list": todo_list}
    return render(request, "todos/list.html", context)


def __str__(self):
    return self.name, self.task


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list": todo_list

    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.author = request.user
            todo_list.save()
            return redirect("todos_list_list")
    else:
        form = TodoForm()
        context = {
            "form" :form,
    }
    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=post)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm(instance=post)

    context = {
        "post": post,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return HttpResponseRedirect(reverse("todos_list_list"))

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {"form": form}
    return render(request, "todos/item_create.html", context)


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.list.id)
    else:
        form = TodoItemForm(instance=post)

    context = {
        "post": post,
        "form": form,
    }
    return render(request, "todos/item_edit.html", context)
